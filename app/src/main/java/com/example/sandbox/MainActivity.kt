package com.example.sandbox

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidlib.myAndroidLib

//import com.example.lib.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        println(multiple(5, 10))
//        printDebug()
        myAndroidLib()
    }

    /**
     * xとyを掛けた値を返す
     */
    fun multiple(x: Int, y:Int) = x * y
}

/**
 * x + yを返す
 */
fun add(x: Int, y: Int) = x + y