package com.example.sandbox

import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.ext.truth.app.NotificationActionSubject.assertThat
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.shadows.ShadowLog

@RunWith(AndroidJUnit4::class)
class MyRobolectricTest{
    @Before
    fun setup(){
        ShadowLog.stream = System.out
    }

    @Test
    fun mytest(){
        assertEquals(2, 1+1)
        val scenario = ActivityScenario.launch(MainActivity::class.java)
        scenario.onActivity {
            val value = it.multiple(2, 30)
            assertEquals(50, it.multiple(5, 10))
        }
    }
}